import { Checkbox } from 'antd';
import { X } from 'lucide-react';

import { useDebounce } from '@/shared/hooks/useDebounce';
import item from './index.module.scss';

interface props {
   uid: string;
   task: string;
   completed: boolean;
   deleteTask: (uid: string) => Promise<void>;
   completeUpdate: (uid: string, completed: boolean) => Promise<void>;
}

export function TaskItem({
   uid,
   task,
   completed,
   deleteTask,
   completeUpdate,
}: props) {
   const debounce = useDebounce((completed: boolean) => completeUpdate(uid, completed), 600);

   return (
      <div className={`${item.container} df aic jcsb`}>
         <div style={{ gap: '0 6px' }} className="df aic">
            <Checkbox
               onChange={e => debounce(e.target.checked)}
               defaultChecked={completed}
            />
            <p>{task}</p>
         </div>
         <button className="df" onClick={() => deleteTask(uid)}>
            <X color={'rgb(240, 240, 240)'} size={17} />
         </button>
      </div>
   );
}

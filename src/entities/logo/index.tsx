import { useNavigate } from 'react-router-dom';

import item from './index.module.scss';
import logo from '@public/astral7.svg';

export function Logo() {
   const navigate = useNavigate();

   return (
      <div className={`${item.logo} df aic hov`} onClick={() => navigate('/')}>
         <img src={logo} alt="logo" loading="eager" />
         <h2>
            Astral
            <span>7</span>
         </h2>
      </div>
   );
}

import { Edit, X } from 'lucide-react';
import { KeyboardEvent, useState } from 'react';
import TextArea from 'antd/es/input/TextArea';

import item from './index.module.scss';

interface props {
   mind: string;
   uid: string;
   deleteMind: (uid: string) => Promise<void>;
   updateMind: (uid: string, mind: { mind: string }) => Promise<void>;
}

export function MindItem({
   mind,
   uid,
   deleteMind,
   updateMind,
}: props) {
   const [open, setOpen] = useState(false);

   const updateMindHandler = (e: KeyboardEvent<HTMLTextAreaElement>) => {
      if (e.key === 'Enter') {
         if (e.currentTarget.value !== mind)
            updateMind(uid, { mind: e.currentTarget.value });

         setOpen(false);
      }
   };

   return (
      <div className={item.container}>
         <div className={item.header}>
            <p>#{uid}</p>
         </div>
         <div className={item.content}>
            {open ? (
               <TextArea
                  autoSize
                  defaultValue={mind}
                  onKeyDown={updateMindHandler}
               />
            ) : (
               <p>{mind}</p>
            )}
         </div>
         <div className={`${item.footer} df aic gr10 jcfe`}>
            <button onClick={() => setOpen(prev => !prev)}>
               <Edit size={20} />
            </button>
            <button onClick={() => deleteMind(uid)}>
               <X size={20} />
            </button>
         </div>
      </div>
   );
}

import { observer } from 'mobx-react-lite';
import { User } from 'lucide-react';

import { authApiStore } from '@/shared/store/auth-api';
import cl from './index.module.scss';
import { useNavigate } from 'react-router-dom'

export const Avatar = observer(() => {
	const navigate = useNavigate();
   const { user } = authApiStore;

   return (
      <div onClick={() => navigate('settings')} className="df aic cp">
         {user?.photoURL ? (
            <img
               className={cl.avatar}
               src={user.photoURL}
               alt="avatar"
               width={35}
               height={35}
            />
         ) : (
            <User color="rgb(240, 240, 240)" />
         )}
      </div>
   );
});

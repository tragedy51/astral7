import { useMemo, useState } from 'react';
import { Checkbox, Modal } from 'antd';
import { Clock } from 'lucide-react';
import dayjs from 'dayjs';

import { TaskDetails } from '@/widgets/dashboard/task-details';
import { IColumnTask } from '@/shared/interfaces/IColumn';
import item from './index.module.scss';

export function TaskItem(props: IColumnTask) {
   const [open, setOpen] = useState(false);

   const getDateInfo = useMemo(() => {
      const daysToExpired = dayjs(props.timestamps[1]).diff(dayjs(), 'day');
      const isExpired = dayjs() >= dayjs(props.timestamps[1]);

      if (props.completed) {
         return {
            text: 'Эта карточка выполнена.',
            color: 'rgb(75, 206, 151)',
            iconColor: '#fff',
         };
      }

      if (isExpired) {
         return {
            text: 'Срок карточки истек.',
            color: 'rgba(255, 0, 0, .35)',
            iconColor: '#ff0000',
         };
      }

      return {
         text: daysToExpired <= 2 ? 'Срок карточки скоро истечет.' : 'Истекает не скоро.',
         color: daysToExpired <= 2 ? 'rgba(255, 153, 0, .35)' : '',
         iconColor: daysToExpired <= 2 ? '#ff9900' : '#fff',
      };
   }, [props.timestamps, props.completed]);

   const { color, text, iconColor } = getDateInfo;

   return (
      <>
         <Modal
            width={'45%'}
            centered
            open={open}
            title={
               <div style={{ paddingRight: 15 }}>
                  <p>{props.title}</p>
               </div>
            }
            children={<TaskDetails {...props} />}
            onCancel={() => setOpen(false)}
            okButtonProps={{ style: { display: 'none' } }}
            cancelButtonProps={{ style: { display: 'none' } }}
         />
         <div className={item.container} onClick={() => setOpen(true)}>
            <div className={`${item.main} df fdc`}>
               {props.image && (
                  <img
                     className={item.image}
                     src={props.image}
                     alt="task-image"
                  />
               )}
               <div onClick={e => e.stopPropagation()}>
                  <Checkbox
                     children={props.title}
                     className={`${item.title} cw`}
                     defaultChecked={props.completed}
                  />
               </div>
            </div>
            <div className={`${item.info} df aic jcsb`}>
               {props.timestamps.length > 0 && (
                  <div
                     title={text}
                     className="df aic"
                     style={{ backgroundColor: color }}>
                     <Clock size={12} color={iconColor} />
                     <p>{dayjs(props.timestamps[1]).format('DD MMMM')}</p>
                  </div>
               )}
               <p style={{ backgroundColor: props.priority.bgColor }}>
                  {props.priority.text} priority
               </p>
            </div>
         </div>
      </>
   );
}

import { Dispatch, SetStateAction } from 'react';
import { FieldValues, Path, SubmitHandler, useForm } from 'react-hook-form';
import { Button } from 'antd';
import { ZodType } from 'zod';
import { X } from 'lucide-react';
import { zodResolver } from '@hookform/resolvers/zod';

import { FieldUi } from '../field';

interface props<T> {
   name: Path<T>;
   zodConfig: ZodType<T>;
   setOpen: Dispatch<SetStateAction<boolean>>;
   request: (data: T) => Promise<void>;
}

export function PopoverForm<T extends FieldValues>({
   name,
   request,
   setOpen,
   zodConfig,
}: props<T>) {
   const {
      formState: { errors },
      handleSubmit,
      control,
   } = useForm<T>({
      resolver: zodResolver(zodConfig),
   });

   const submitHandler: SubmitHandler<T> = data => {
      request(data);
      setOpen(false);
   };

   return (
      <form onSubmit={handleSubmit(submitHandler)}>
         <FieldUi
            name={name}
            required={false}
            showTitle={false}
            control={control}
            // @ts-ignore
            error={errors[name]?.message}
         />
         <div style={{ marginTop: 5 }} className="df aic gr10">
            <Button htmlType="submit" type="primary">
               Create
            </Button>
            <button type="button" onClick={() => setOpen(false)}>
               <X color={'rgb(240, 240, 240)'} />
            </button>
         </div>
      </form>
   );
}

import { Input } from 'antd';
import { Control, Controller, FieldValues, Path } from 'react-hook-form';

import cl from './index.module.scss';

interface props<T extends FieldValues> {
   name: Path<T>;
   error: string | undefined;
   control: Control<T>;
   required: boolean;
   showTitle?: boolean;
   placeholder?: string;
}

export function FieldUi<T extends FieldValues>({
   name,
   error,
   control,
   required,
   showTitle = true,
   placeholder,
}: props<T>) {
   return (
      <label className={cl.container}>
         {showTitle && (
            <p className="df aic">
               {name} {required && <sup>*</sup>}
            </p>
         )}
         <Controller
            name={name}
            control={control}
            render={({ field }) => <Input {...field} placeholder={placeholder} />}
         />
         <span>{error}</span>
      </label>
   );
}

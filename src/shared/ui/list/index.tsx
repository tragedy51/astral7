import { ReactNode } from 'react';

interface props<T> {
   list: T[] | undefined;
   callback: (listItem: T, index: Partial<number>) => ReactNode;
}

export function List<T>({ list, callback }: props<T>) {
   return list?.map(callback);
}

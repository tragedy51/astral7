import { useCallback, useRef } from 'react'

export function useDebounce<T extends (...args: any) => void> (callback: T, delay: number) {
	const timeout = useRef<ReturnType<typeof setTimeout>>();

	return useCallback((...args: any[]) => {
		if (timeout.current) clearTimeout(timeout.current);

		timeout.current = setTimeout(() => callback(...args), delay);
	}, [callback, delay])
}
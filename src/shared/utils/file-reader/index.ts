import { RcFile } from 'antd/es/upload';
import { notification } from 'antd';

export const base64 = (img: RcFile, callback: (url: string) => void) => {
   const reader = new FileReader();
   reader.addEventListener('load', () => callback(reader.result as string));
   reader.readAsDataURL(img);
};

export const beforeUpload = (file: RcFile) => {
   const isCorrect = file.type === 'image/jpeg' || file.type === 'image/png';
   const isLt2m = file.size / 1024 / 1024 < 2;

   if (!isCorrect) notification.error({ message: 'You can only upload jpg/png file' });
   if (!isLt2m) notification.error({ message: 'Image must smaller than 2 mb!' });

   return isCorrect && isLt2m;
};

export const request = ({ onSuccess }: { onSuccess?: any }) =>
	setTimeout(() => onSuccess('ok'), 0);

import { z } from 'zod';

import { taskSchema } from '../schemas/dashboard/create-task';

export interface IColumnTask extends z.infer<typeof taskSchema> {
   completed: boolean;
   id: string;
   parentId: string;
}

export interface IColumn {
   title: string;
   tasks: IColumnTask[] | null;
   uid: string;
}
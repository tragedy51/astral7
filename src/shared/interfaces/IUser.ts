import { User } from 'firebase/auth';

export interface IUser
   extends Pick<
      User,
      'displayName' | 'email' | 'photoURL' | 'uid'
   > {
   description?: string;
   birthday?: string;
}

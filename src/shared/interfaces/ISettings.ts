export interface ISettingToUpdate {
	key: string;
	value: string;
}
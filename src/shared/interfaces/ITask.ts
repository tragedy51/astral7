export interface ITask {
	uid: string;
	task: string;
	completed: boolean;
}
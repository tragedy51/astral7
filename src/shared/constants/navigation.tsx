import {
   Brain,
   ClipboardList,
   LayoutDashboard,
   Settings,
   User,
} from 'lucide-react';

export const navigation = [
   { icon: <LayoutDashboard />, text: 'Dashboard', path: '/' },
   { icon: <ClipboardList />, text: 'Tasks', path: '/tasks' },
   { icon: <Brain />, text: 'Minds', path: '/minds' },
   { icon: <User />, text: 'Users', path: '/users' },
   { icon: <Settings />, text: 'Settings', path: '/settings' },
];

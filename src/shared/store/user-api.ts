import makeAutoObservable from 'mobx-store-inheritance';

import { IUser } from '../interfaces/IUser';
import { Api } from './common/api';
import { doc, getDoc } from 'firebase/firestore'
import { db } from '@/app/_providers/firebase'
import { runInAction } from 'mobx'

class UserApiStore extends Api<IUser> {
   constructor() {
		super('users');
		makeAutoObservable(this);
	}

	user = {} as IUser;

	getUser = async (id: string) => {
		try {
			this.setLoading(true);
			const res = await getDoc(doc(db, this.path, id));
			runInAction(() => {
				this.setUser(res.data() as IUser);
				this.setLoading(false);
			})
		} catch (e) {
			this.setLoading(false);
			throw new Error(`An error was occured when get user: ${e}`);
		}
	}

	setUser = (user: IUser) => this.user = user;
}

export const userApiStore = new UserApiStore();

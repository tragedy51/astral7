import { makeAutoObservable, runInAction } from 'mobx';
import { signInWithEmailAndPassword, signOut } from 'firebase/auth';
import { createUserWithEmailAndPassword } from 'firebase/auth';
import { FirebaseError } from 'firebase/app';
import { doc, getDoc, setDoc } from 'firebase/firestore';

import { auth, db } from '@/app/_providers/firebase';
import { IUser } from '../interfaces/IUser';
class AuthApiStore {
   constructor() { makeAutoObservable(this) }

   // ========================================== AUTH ==========================================

   // ALL AUTH STATES
   loading = false;
   user?: IUser;
   status?: string;
   error?: string;

   // ALL AUTH ACTIONS
   signUp = async (email: string, password: string, username?: string) => {
      try {
         this.setStatus('pending');
         const { user } = await createUserWithEmailAndPassword(auth, email, password);
         localStorage.setItem('token', user.uid);
         await setDoc(doc(db, 'users', localStorage.getItem('token')!), {
            displayName: username || null,
            email,
            uid: user.uid
         });
         runInAction(() => {
            this.setUser(user);
            this.setStatus('success');
         });
      } catch (e) {
         this.setStatus('error');
         if (e instanceof FirebaseError) this.setError(e.message);
      }
   };

   signIn = async (email: string, password: string) => {
      try {
         this.setStatus('pending');
         const { user } = await signInWithEmailAndPassword(auth, email, password);
         localStorage.setItem('token', user.uid);
         runInAction(() => {
            this.setUser(user);
            this.setStatus('success');
         });
      } catch (e) {
         this.setStatus('error');
         if (e instanceof FirebaseError) this.setError(e.message);
      }
   };

   get_me = async () => {
      if (!localStorage.getItem('token')) return;
      try {
         this.setLoading(true);
         const res = await getDoc(doc(db, 'users', localStorage.getItem('token')!));
         runInAction(() => {
            this.setUser(res.data() as IUser);
            this.setLoading(false);
         });
      } catch (e) {
         throw new Error(`An error was occured when getting user: ${e}`);
      }
   };

   // ALL AUTH STATES MOVES
   setLoading = (val: boolean) => this.loading = val;
   setUser = (user: IUser | undefined) => this.user = user;
   setError = (error: string | undefined) => this.error = error;
   setStatus = (status: string | undefined) => this.status = status;

   // ALL RESET ACTIONS
   logout = async () => {
      await signOut(auth);
      localStorage.removeItem('token');
      this.setUser(undefined);
   };

   reset = () => {
      this.setError(undefined);
      this.setStatus(undefined);
   };
}

export const authApiStore = new AuthApiStore();

import { addDoc, collection, deleteDoc, doc, getDocs, updateDoc } from 'firebase/firestore';
import { runInAction } from 'mobx';

import { db } from '@/app/_providers/firebase';

export class Api<T> {
   constructor(path: string) { this.path = path }

   // ========================================= API REQUESTS =======================================

   // ALL API STATES
   path: string;
   loading = false;
   data: T[] = [];

   // ALL API ACTIONS
   getData = async () => {
      try {
			this.setLoading(true);
         const { docs } = await getDocs(collection(db, this.path));
         runInAction(() => {
				this.setData(docs.map(doc => ({ ...doc.data(), uid: doc.id } as T)));
				this.setLoading(false);
			})
      } catch (e) {
			this.setLoading(false);
         throw new Error(`An error was occured when get data: ${e}`);
      }
   };

   createData = async <R extends Record<string, any>>(obj: R) => {
      try {
         this.setLoading(true);
         await addDoc(collection(db, this.path), obj);
			this.setLoading(false);
      } catch (e) {
         this.setLoading(false);
         throw new Error(`An error was occured when create data: ${e}`);
      }
   };

   updateData = async <R extends Record<string, any>>(id: string, obj: R) => {
      try {
         this.setLoading(true);
			await updateDoc(doc(db, this.path, id), obj);
			this.setLoading(false);
      } catch (e) {
         this.setLoading(false);
         throw new Error(`An error was occured when update data: ${e}`);
      }
   };

   deleteData = async (id: string) => {
      try {
         this.setLoading(true);
         await deleteDoc(doc(db, this.path, id));
			this.setLoading(false);
      } catch (e) {
         this.setLoading(false);
         throw new Error(`An error was occured when delete data: ${e}`);
      }
   };

   // ALL STATES MOVES
   setData = (data: T[]) => this.data = data;
   setLoading = (val: boolean) => this.loading = val;
}

import makeAutoObservable from 'mobx-store-inheritance';
import { reaction } from 'mobx';

import { IMind } from '@/shared/interfaces/IMind';
import { Api } from './common/api';
import { token } from '../constants/token';

class MindsApiStore extends Api<IMind> {
   constructor() {
      super(`users/${token}/minds`);
      makeAutoObservable(this);

      reaction(
         () => this.loading,
         loading => loading && this.getData(),
      );
   }
}

export const mindsApiStore = new MindsApiStore();

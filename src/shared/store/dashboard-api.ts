import { arrayRemove, arrayUnion, doc, updateDoc } from 'firebase/firestore';
import { reaction } from 'mobx';
import { nanoid } from 'nanoid';
import makeAutoObservable from 'mobx-store-inheritance';

import { db } from '@/app/_providers/firebase'
import { IColumnTask } from '../interfaces/IColumn';
import { IColumn } from '../interfaces/IColumn';
import { token } from '../constants/token';
import { Api } from './common/api';

class DashboardApiStore extends Api<IColumn> {
   constructor() {
      super(`users/${token}/columns`);
      makeAutoObservable(this);

      reaction(
         () => this.loading,
         loading => loading && this.getData(),
      );
   }

   // ===================================== ALL  TASKS ====================================

   // ALL DASHBOARD TASKS ACTIONS
   createTask = async (columnId: string, task: Omit<IColumnTask, 'completed' | 'id'>) => {
      await updateDoc(doc(db, this.path, columnId), {
         tasks: arrayUnion({
            ...task,
            id: nanoid(),
            completed: false,
            parentId: columnId
         })
      })
   };

   deleteTask = async (columnId: string, taskId: string) => {
      const column = this.data.find(item => item.uid === columnId);
      const task = column?.tasks?.find(task => task.id === taskId);

      try {
         this.setLoading(true);
         await updateDoc(doc(db, this.path, columnId), {
            tasks: arrayRemove(task)
         });
         this.setLoading(false);
      } catch (e) {
         this.setLoading(false);
         throw new Error(`An error was occured when delete task: ${e}`);
      }
   }

   updateTaskComplete = async (columnId: string, taskId: string) => {
      await updateDoc(doc(db, this.path, columnId), {

      })
   }
}

export const dashboardApiStore = new DashboardApiStore();

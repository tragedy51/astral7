import { makeAutoObservable } from 'mobx';
import { notification } from 'antd';
import { updateEmail, updatePassword, User } from 'firebase/auth';
import { doc, updateDoc } from 'firebase/firestore';
import { FirebaseError } from 'firebase/app';

import { db } from '@/app/_providers/firebase';
import { ICredentials } from '../schemas/settings';
import { ISettingToUpdate } from '../interfaces/ISettings';
import { token } from '../constants/token';

class SettingsApiStore {
   constructor() {
      makeAutoObservable(this);
   }

   // ========================================= SETTINGS ========================================

	// ALL SETTINGS STATES
	status?: string;

   // ALL SETTINGS ACTIONS
	updatePublicData = async ({ key, value }: ISettingToUpdate) => {
		try {
			this.setStatus('pending');
			await updateDoc(doc(db, 'users', token!), { [key]: value });
			this.setStatus('success');
		} catch (e) {
			this.setStatus('error');
			if (e instanceof FirebaseError) notification.error({ message: e.code });
		}
	}

	updatePrivateData = async (user: User, { email, password }: ICredentials) => {
		try {
			this.setStatus('pending');
			if (email) {
				await updateEmail(user, email);
				await updateDoc(doc(db, 'users', token!), { email });
			}
			if (password) await updatePassword(user, password);
			this.setStatus('success');
		} catch (e) {
			this.setStatus('error');
			if (e instanceof FirebaseError) notification.error({ message: e.code });
		}
	}

	// ALL SETTINGS MOVES
	setStatus = (status: string) => this.status = status;
}

export const settingsApiStore = new SettingsApiStore();

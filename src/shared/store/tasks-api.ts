import { reaction } from 'mobx';
import { doc, updateDoc } from 'firebase/firestore';
import makeAutoObservable from 'mobx-store-inheritance';

import { ITask } from '../interfaces/ITask';
import { db } from '@/app/_providers/firebase';
import { token } from '../constants/token';
import { Api } from './common/api';

class TasksApiStore extends Api<ITask> {
   constructor() {
      super(`users/${token}/tasks`);
      makeAutoObservable(this);

      reaction(
         () => this.loading,
         loading => loading && this.getData(),
      );
   }

   // ======================================= PAGINATION =======================================

   // ALL STATES
   currentPage = 1;
   from = 1;
   to = 10;

   // ALL ACTIONS
   changeCurrentPage = (page: number) => {
      if (page > this.currentPage) {
         this.to = 10 * page;
         this.from = this.to - 10;
      } else {
         this.to -= 10;
         this.from -= 10;
      }

      this.currentPage = page;
   };

   // TASK COMPLETE UPDATE
   completeUpdate = async (uid: string, completed: boolean) => {
      await updateDoc(doc(db, this.path, uid), { completed });
   }
}

export const tasksApiStore = new TasksApiStore();

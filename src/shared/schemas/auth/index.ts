import { z } from 'zod';

export const signUpSchema = z
   .object({
      username: z
         .string()
         .min(3, { message: 'Username must be least at 3 bytes' })
         .max(20, { message: 'Username is too long' })
         .optional(),
      email: z
         .string()
         .min(1, { message: 'This field is required' })
         .email({ message: 'Invalid email address' }),
      password: z
         .string()
         .min(6, { message: 'Password must be least at 6 bytes' }),
      confirmPassword: z.string().min(1, { message: 'This field is required' }),
   })
   .refine(data => data.password === data.confirmPassword, {
      path: ['confirmPassword'],
      message: 'Passwords do not match',
   });

export interface ISignUp extends z.infer<typeof signUpSchema> {}

export const signInSchema = z.object({
   email: z.string().min(1, { message: 'This field is required' }).email(),
   password: z.string().min(1, { message: 'This field is required' }),
});

export interface ISignIn extends z.infer<typeof signInSchema> {}

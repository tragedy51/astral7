import { z } from 'zod';

export const taskSchema = z.object({
   title: z
      .string()
      .min(3, { message: 'Title must be least at 3 bytes' })
      .max(100, { message: 'Title length is too long' }),
   description: z
      .string()
      .min(10, { message: 'Description must be least at 10 bytes' })
      .max(500, { message: 'Description is too long' })
      .optional(),
   timestamps: z.array(z.number()).length(2).optional(),
   image: z.string().optional(),
   priority: z.object({
      text: z.string(),
      level: z.number(),
      bgColor: z.string(),
   }),
}).transform(data => ({
   ...data,
   description: data.description ?? null,
   image: data.image ?? null,
   timestamps: data.timestamps ?? []
}));

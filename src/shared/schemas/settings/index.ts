import { z } from 'zod';

export const credentialsSchema = z.object({
	email: z.string().email().optional(),
	password: z.string().min(6, {message: 'Password must be least at 6 bytes'}).optional()
}).refine(({ email, password }) => email || password, {
	path: ['email'],
	message: `You skipped all fields. This not allowed`
});

export interface ICredentials extends z.infer<typeof credentialsSchema> {}
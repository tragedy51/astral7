import { useEffect, useState } from 'react';
import { Button, Popover } from 'antd';
import { observer } from 'mobx-react-lite';
import { z } from 'zod';
import Skeleton from 'react-loading-skeleton';

import { mindsApiStore } from '@/shared/store/minds-api';
import { PopoverForm } from '@/shared/ui/popover-form';
import { MindItem } from '@/entities/minds/mind-item';
import { List } from '@/shared/ui/list';

export const Minds = observer(() => {
   const [open, setOpen] = useState(false);
   const { getData, updateData, createData, data, deleteData, loading } = mindsApiStore;

   useEffect(() => { getData() }, []);

   const configForCreateMind = z.object({
      mind: z.string().min(3, { message: 'Mind must be least at 3 bytes' }),
   });

   if (loading) {
      return (
         <div>
            <Button type="primary">Create Mind</Button>
            <Skeleton style={{ marginTop: 15 }} height={125} count={4} />
         </div>
      );
   }

   return (
      <div>
         <div>
            <Button onClick={() => setOpen(prev => !prev)} type="primary">
               Create Mind
            </Button>
            <Popover
               open={open}
               arrow={false}
               placement="rightBottom"
               content={
                  <PopoverForm
                     name="mind"
                     setOpen={setOpen}
                     zodConfig={configForCreateMind}
                     request={createData}
                  />
               }
            />
         </div>
         <div>
            <List
               list={data}
               callback={item => (
                  <MindItem
                     key={item.uid}
                     {...item}
                     deleteMind={deleteData}
                     updateMind={updateData}
                  />
               )}
            />
         </div>
      </div>
   );
});

import { useEffect, useState } from 'react';
import { Popover } from 'antd';
import { observer } from 'mobx-react-lite';
import { Plus } from 'lucide-react';
import { z } from 'zod';
import Skeleton from 'react-loading-skeleton';

import { List } from '@/shared/ui/list';
import { Column } from '@/widgets/dashboard/column';
import { PopoverForm } from '@/shared/ui/popover-form';
import { dashboardApiStore } from '@/shared/store/dashboard-api';
import board from './index.module.scss';

export const Dashboard = observer(() => {
   const [open, setOpen] = useState(false);
   const { getData, updateData, createData, data, deleteData, loading } = dashboardApiStore;

   useEffect(() => { getData() }, []);

   const configForCreateColumn = z.object({
      title: z
         .string()
         .min(3, { message: 'Title of column must be least at 3 bytes' })
         .max(20, { message: 'Title of column is too long' }),
   });

   return (
      <div className={`${board.container} df gr10`}>
         {loading ? (
            <Skeleton inline width={295} height={90} count={5} />
         ) : (
            <>
               <List
                  list={data}
                  callback={card => (
                     <Column
                        {...card}
                        key={card.uid}
                        deleteColumn={deleteData}
                        updateColumnTitle={updateData}
                     />
                  )}
               />
               <button
                  onClick={() => setOpen(prev => !prev)}
                  className={`${board.addColumn} df jcc aic cw`}>
                  <Plus size={18} />
                  Add column
               </button>
               <Popover
                  arrow={false}
                  open={open}
                  placement="rightTop"
                  content={
                     <PopoverForm
                        name="title"
                        setOpen={setOpen}
                        request={createData}
                        zodConfig={configForCreateColumn}
                     />
                  }
               />
            </>
         )}
      </div>
   );
});

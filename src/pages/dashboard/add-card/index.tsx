import { useLocation, useNavigate } from 'react-router-dom';
import { Controller, ControllerRenderProps, SubmitHandler, useForm } from 'react-hook-form';
import { zodResolver } from '@hookform/resolvers/zod';
import { observer } from 'mobx-react-lite';
import { ArrowRight, CalendarRange, ImagePlus } from 'lucide-react';
import { Button, DatePicker, Radio, Upload } from 'antd';
import { RcFile, UploadChangeParam, UploadFile } from 'antd/es/upload';
import { File } from 'buffer';
import TextArea from 'antd/es/input/TextArea';
import dayjs from 'dayjs';

import { request, base64, beforeUpload } from '@/shared/utils/file-reader';
import { taskSchema } from '@/shared/schemas/dashboard/create-task';
import { dashboardApiStore } from '@/shared/store/dashboard-api';
import { IColumnTask } from '@/shared/interfaces/IColumn';
import { FieldUi } from '@/shared/ui/field';
import card from './index.module.scss';

export const AddCard = observer(() => {
   const { pathname } = useLocation();
   const navigate = useNavigate();
   const { createTask } = dashboardApiStore;
   const {
      formState: { errors },
      handleSubmit,
      control,
      getValues,
   } = useForm<IColumnTask>({
      resolver: zodResolver(taskSchema),
      defaultValues: {
         priority: { text: 'Low', bgColor: '#33CDC7', level: 1 }
      },
   });

   const submitHandler: SubmitHandler<IColumnTask> = data => {
      const id = pathname.match(/^\/([^/]+)\//)!;

      createTask(id[1], data);
      navigate('/');
   };

   const uploadImage = (
      { file }: UploadChangeParam<UploadFile<File>>,
      { onChange }: ControllerRenderProps<IColumnTask, 'image'>,
   ) => {
      if (file.status === 'uploading') return;
      if (file.status === 'done') {
         base64(file.originFileObj as RcFile, url => onChange(url));
      }
   };

   const priorities = [
      { text: 'Low', bgColor: '#33CDC7', level: 1 },
      { text: 'Medium', bgColor: '#FFA900', level: 2 },
      { text: 'High', bgColor: '#FF4040', level: 3 },
   ];

   return (
      <form
         onSubmit={handleSubmit(submitHandler)}
         className={`${card.container} df fdc`}>
         <div className={`${card.main} df gr10`}>
            <Controller
               name="image"
               control={control}
               render={({ field }) => (
                  <Upload
                     name="image"
                     listType="picture"
                     showUploadList={false}
                     customRequest={request}
                     beforeUpload={beforeUpload}
                     className={`${card.upload} df jcc aic fdc cw`}
                     onChange={res => uploadImage(res, field)}>
                     {getValues().image ? (
                        // @ts-ignore
                        <img src={getValues().image} alt="image" />
                     ) : (
                        <>
                           <p className="tac">
                              <ImagePlus size={45} />
                           </p>
                           <p className={card.text}>
                              Click to upload image for task in dashboard!
                           </p>
                        </>
                     )}
                  </Upload>
               )}
            />
            <div className={card.fields}>
               <FieldUi
                  control={control}
                  error={errors.title?.message}
                  name="title"
                  required
               />
               <label className={`${card.description} df fdc`}>
                  Description
                  <Controller
                     name="description"
                     control={control}
                     // @ts-ignore
                     render={({ field }) => <TextArea {...field} />}
                  />
                  <span>{errors.description?.message}</span>
               </label>
            </div>
         </div>
         <label className={`${card.datepicker} df fdc`}>
            Select a date
            <Controller
               name="timestamps"
               control={control}
               render={({ field }) => (
                  <DatePicker.RangePicker
                     showTime
                     placement="topLeft"
                     format={'DD.MM.YYYY | HH:mm:ss'}
                     placeholder={['Start date', 'Expired date']}
                     separator={<ArrowRight color="#f0f0f0" size={18} />}
                     suffixIcon={<CalendarRange color="#f0f0f0" size={20} />}
                     onChange={dates =>
                        field.onChange(
                           // @ts-ignore
                           dates?.map(date => dayjs(date).valueOf()),
                        )
                     }
                  />
               )}
            />
            <span>{errors.timestamps?.message}</span>
         </label>
         <label className="df fdc">
            Task Priority
            <Controller
               name="priority"
               control={control}
               render={({ field }) => (
                  <Radio.Group
                     defaultValue={0}
                     className={`${card.radios} df fdc`}>
                     {priorities.map((p, i) => (
                        <Radio
                           key={i}
                           value={i}
                           children={p.text}
                           onChange={() => field.onChange(p)}
                        />
                     ))}
                  </Radio.Group>
               )}
            />
         </label>
         <Button
            className={`${card.submitBtn} cw`}
            htmlType="submit"
            type="primary">
            Create
         </Button>
      </form>
   );
});

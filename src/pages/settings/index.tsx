import { useEffect } from 'react';
import { observer } from 'mobx-react-lite';

import { auth } from '@/app/_providers/firebase';
import { settingsApiStore } from '@/shared/store/settings-api';
import { authApiStore } from '@/shared/store/auth-api';
import { UserInfo } from '@/widgets/settings/user-info';
import { Description } from '@/widgets/settings/description';
import { Credentials } from '@/widgets/settings/credentials';
import { UserDates } from '@/widgets/settings/user-dates';
import { Footer } from '@/widgets/settings/footer';
import set from './index.module.scss';
import Skeleton from 'react-loading-skeleton';

export const Settings = observer(() => {
   const { user, get_me, loading } = authApiStore;
   const { updatePublicData, updatePrivateData, status } = settingsApiStore;

   useEffect(() => { status === 'success' && get_me() }, [status]);

   if (loading) {
      return (
         <div className={`${set.container} df fdc`}>
            <div className={`${set.mainInfo} df`}>
               <Skeleton className={set.skeleton} height={210} />
               <div style={{ gap: '15px 0' }} className="w100 df fdc">
                  <Skeleton height={143} />
                  <Skeleton height={50} />
               </div>
            </div>
            <Skeleton height={250} />
            <Footer />
         </div>
      );
   }

   return (
      <div className={`${set.container} df fdc`}>
         <div className={`${set.mainInfo} df`}>
            <UserInfo updatePublicData={updatePublicData} user={user} />
            <div style={{ gap: '15px 0' }} className="w100 df fdc">
               <Description
                  updatePublicData={updatePublicData}
                  description={user?.description}
               />
               <UserDates
                  updatePublicData={updatePublicData}
                  createdAt={auth.currentUser?.metadata.creationTime}
                  birthday={user?.birthday}
               />
            </div>
         </div>
         <Credentials
            user={auth.currentUser}
            loading={status === 'pending'}
            updatePrivateData={updatePrivateData}
         />
         <Footer />
      </div>
   );
});

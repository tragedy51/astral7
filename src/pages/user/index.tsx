import { useEffect, useState } from 'react';
import { useLocation, useNavigate } from 'react-router-dom';
import { observer } from 'mobx-react-lite';
import { Info, User as UserIcon } from 'lucide-react';
import { Button, Modal } from 'antd';
import Skeleton from 'react-loading-skeleton';

import { userApiStore } from '@/shared/store/user-api';
import { token } from '@/shared/constants/token';
import { MoreInfo } from '@/widgets/user/more-info';
import cl from './index.module.scss';

export const User = observer(() => {
   const [open, setOpen] = useState(false);
   const navigate = useNavigate();
   const { pathname } = useLocation();
   const { user, getUser, loading } = userApiStore;

   useEffect(() => {
      const id = pathname.match(/user\/([a-zA-Z0-9]+)/)![1];

      if (id === token) navigate('/settings');
      else getUser(id);
   }, []);

   if (loading) {
      return (
         <div className={cl.container}>
            <Button type="primary" onClick={() => navigate(-1)}>
               Back
            </Button>
            <div className={`${cl.user} df aic gr10`}>
               <Skeleton circle width={120} height={120} />
               <div className="w100">
                  <Skeleton width={130} height={20} />
                  <div style={{ marginTop: 5 }}>
                     <Skeleton style={{ marginTop: 3 }} height={15} count={3} />
                     <button className={`${cl.more} df aic`}>
                        <Info size={18} />
                        More Info
                     </button>
                  </div>
               </div>
            </div>
         </div>
      );
   }

   return (
      <div className={cl.container}>
         <Modal
            open={open}
            title={user?.displayName || 'Guest'}
            onCancel={() => setOpen(false)}
            children={
               <MoreInfo
                  photoURL={user?.photoURL}
                  uid={user?.uid}
                  birthday={user?.birthday}
                  description={user?.description}
               />
            }
            okButtonProps={{ style: { display: 'none' } }}
            cancelButtonProps={{ style: { display: 'none' } }}
         />
         <Button type="primary" onClick={() => navigate(-1)}>
            Back
         </Button>
         <div className={`${cl.user} df aic gr10`}>
            {user?.photoURL ? (
               <img
                  src={user.photoURL}
                  alt="avatar"
                  width={120}
                  height={120}
               />
            ) : (
               <UserIcon size={120} color="rgb(240, 240, 240)" />
            )}
            <div className={cl.main}>
               <h1>{user?.displayName || 'Guest'}</h1>
               <p>{user?.description || 'Not specified :('}</p>
               <button
                  onClick={() => setOpen(true)}
                  className={`${cl.more} df aic`}>
                  <Info size={18} />
                  More Info
               </button>
            </div>
         </div>
      </div>
   );
});

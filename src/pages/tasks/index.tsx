import { useEffect, useState } from 'react';
import { observer } from 'mobx-react-lite';
import { Button, Pagination, Popover } from 'antd';
import { ListTodo } from 'lucide-react';
import { z } from 'zod';
import Skeleton from 'react-loading-skeleton';

import { TaskItem } from '@/entities/tasks/task-item';
import { tasksApiStore } from '@/shared/store/tasks-api';
import { PopoverForm } from '@/shared/ui/popover-form';
import { List } from '@/shared/ui/list';
import task from './index.module.scss';

export const Tasks = observer(() => {
   const [open, setOpen] = useState(false);
   const {
      to,
      from,
      data,
      loading,
      getData,
      createData,
      deleteData,
      currentPage,
      completeUpdate,
      changeCurrentPage,
   } = tasksApiStore;

   useEffect(() => { getData() }, []);

   const configForCreateTask = z.object({
      task: z
         .string()
         .min(3, { message: 'Task length must be least 3 bytes' })
         .max(224, { message: 'Task length is too long' }),
   });

   return (
      <div className={task.container}>
         <div>
            <Button
               onClick={() => setOpen(prev => !prev)}
               style={{ maxWidth: 'fit-content' }}
               type="primary">
               Create Task
            </Button>
            <Popover
               open={open}
               arrow={false}
               placement="rightBottom"
               content={
                  <PopoverForm
                     name="task"
                     setOpen={setOpen}
                     request={createData}
                     zodConfig={configForCreateTask}
                  />
               }
            />
         </div>
         <div className={task.content}>
            <div className={`${task.title} df aic`}>
               <ListTodo />
               <h1>Tasks</h1>
            </div>
            {loading ? (
               <div>
                  <Skeleton count={10} height={42} style={{ marginTop: 5 }} />
               </div>
            ) : (
               <div className={`${task.items} df fdc`}>
                  <List
                     list={data.filter((_, i) => from <= i + 1 && i + 1 <= to)}
                     callback={item => (
                        <TaskItem
                           {...item}
                           key={item.uid}
                           deleteTask={deleteData}
                           completeUpdate={completeUpdate}
                        />
                     )}
                  />
               </div>
            )}
         </div>
         <div style={{ marginTop: 7 }} className="df jcc">
            {loading ? (
               <Skeleton width={300} height={35} />
            ) : (
               <Pagination
                  responsive
                  total={data.length}
                  current={currentPage}
                  onChange={changeCurrentPage}
               />
            )}
         </div>
      </div>
   );
});

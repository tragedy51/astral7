import { useEffect } from 'react';
import { useNavigate, useSearchParams } from 'react-router-dom';
import { notification } from 'antd';
import { observer } from 'mobx-react-lite';

import { authApiStore } from '@/shared/store/auth-api';
import { SignIn } from '@/widgets/auth/signIn';
import { SignUp } from '@/widgets/auth/signUp';
import { Logo } from '@/entities/logo';
import auth from './index.module.scss';

export const Auth = observer(() => {
   const { signUp, signIn, status, error, reset } = authApiStore;
   const [params, setParams] = useSearchParams();
   const navigate = useNavigate();

   useEffect(() => {
      switch (status) {
         case 'pending':
            notification.info({ message: 'Waiting for response...' });
            break;
         case 'success':
            notification.success({ message: 'Success!' });
            reset();
            navigate('/');
            break;
         case 'error':
            notification.error({ message: `Something went wrong.. ${error}` });
            break;
      }
   }, [status]);

   const authBtns = [
      { text: 'Sign In', params: '' },
      { text: 'Sign Up', params: 'method=signup' },
   ];

   return (
      <div style={{ padding: '0 15px', height: '100%' }} className="df jcc aic">
         <div className={`${auth.form} df fdc cw w100`}>
            <Logo />
            <div className="df gr10">
               {authBtns.map((btn, i) => (
                  <button
                     key={i}
                     className={
                        btn.params === params.toString()
                           ? auth.active
                           : auth.btn
                     }
                     onClick={() => setParams(btn.params)}
                  >
                     {btn.text}
                  </button>
               ))}
            </div>
            {params.toString() === '' ? (
               <SignIn status={status} signIn={signIn} />
            ) : (
               <SignUp status={status} signUp={signUp} />
            )}
         </div>
      </div>
   );
});

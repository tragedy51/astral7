import { useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import { observer } from 'mobx-react-lite';
import { Link, User } from 'lucide-react';
import { List } from 'antd';
import Skeleton from 'react-loading-skeleton';

import { userApiStore } from '@/shared/store/user-api';
import { token } from '@/shared/constants/token';
import cl from './index.module.scss';

export const Users = observer(() => {
   const navigate = useNavigate();
   const { loading, data, getData } = userApiStore;

   useEffect(() => { getData() }, []);

   return (
      <div className={cl.container}>
         {loading ? (
            <Skeleton height={55} count={10} style={{ marginTop: 10 }} />
         ) : (
            <List
               dataSource={data}
               renderItem={user => (
                  <div
                     key={user.uid}
                     className={`${cl.item} ${
                        token === user.uid && cl.me
                     } df jcsb aic`}>
                     <div className="df aic gr10">
                        {user.photoURL ? (
                           <img
                              src={user.photoURL}
                              alt="avatar"
                              width={40}
                              height={40}
                           />
                        ) : (
                           <User size={40} />
                        )}
                        <p>{user.displayName || 'Guest'}</p>
                     </div>
                     <button
                        onClick={() =>
                           navigate(
                              token === user.uid
                                 ? '/settings'
                                 : `/user/${user.uid}`
                           )
                        }>
                        <Link />
                     </button>
                  </div>
               )}
            />
         )}
      </div>
   );
});

import ReactDOM from 'react-dom/client';
import { SkeletonTheme } from 'react-loading-skeleton';
import { ConfigProvider } from 'antd';

import { AppRouter } from './routes/AppRouter';
import { config } from './_providers/antd';
import 'react-loading-skeleton/dist/skeleton.css';
import './assets/styles/global.scss';

ReactDOM.createRoot(document.getElementById('root')!).render(
   <SkeletonTheme baseColor="#202020" highlightColor="#444">
      <ConfigProvider theme={{ components: config }}>
         <AppRouter />
      </ConfigProvider>
   </SkeletonTheme>,
);

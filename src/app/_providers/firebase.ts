import { initializeApp } from 'firebase/app';
import { getAuth } from 'firebase/auth';
import { getFirestore } from 'firebase/firestore';

const app = initializeApp({
   apiKey: 'AIzaSyDd_f9NFkN_EFcaDx760LTgzINkKlE4uto',
   authDomain: 'astral7-eaeae.firebaseapp.com',
   projectId: 'astral7-eaeae',
   storageBucket: 'astral7-eaeae.appspot.com',
   messagingSenderId: '611837485395',
   appId: '1:611837485395:web:1b5a3a971ae76a99d101a6',
});

export const db = getFirestore(app);
export const auth = getAuth();
const Input = {
   colorText: 'rgb(240, 240, 240)',
   colorTextPlaceholder: 'rgba(220, 220, 220, .6)',
   hoverBorderColor: 'rgba(245, 245, 245, .45)',
   activeBorderColor: 'rgba(245, 245, 245, .6)',
   colorBgContainer: '#22272B',
   colorBorder: '#A6C5E229',
   borderRadius: 5,
};

const Button = {
   fontWeight: 500,
   defaultColor: 'rgb(240, 240, 240)',
   colorPrimary: 'linear-gradient(50deg, rgb(228, 129, 0) 27%, rgb(191, 0, 220) 100%)',
   colorPrimaryHover: 'linear-gradient(50deg, rgb(228, 129, 0) 27%, rgb(191, 0, 220) 100%)',
   colorPrimaryActive: 'linear-gradient(50deg, rgb(223, 124, 0) 27%, rgb(186, 0, 215) 100%)',
   colorTextDisabled: 'rgb(240, 240, 240)',
   borderColorDisabled: 'rgba(245, 245, 245, .45)',
   borderRadius: 5,
};

const DatePicker = {
   borderRadius: 5,
   colorBgContainer: '#22272B',
   colorText: 'rgb(240, 240, 240)',
   colorPrimary: 'linear-gradient(50deg, rgb(228, 129, 0) 27%, rgb(191, 0, 220) 100%)',
   colorBorder: '#A6C5E229',
   colorTextPlaceholder: 'rgba(220, 220, 220, .6)',
   colorIcon: 'rgb(240, 240, 240)',
   colorIconHover: 'rgba(240, 240, 240, .5)',
   cellActiveWithRangeBg: 'rgba(22, 26, 29, .7)',
   colorBgElevated: '#22272B',
   controlItemBgActive: 'rgba(22, 26, 29, .7)',
   colorTextHeading: 'rgb(240, 240, 240)',
   cellHoverBg: 'rgba(22, 26, 29, .5)',
   activeBorderColor: 'rgba(245, 245, 245, .6)',
   hoverBorderColor: 'rgba(245, 245, 245, .45)',
   motionDurationSlow: '0.2s',
   motionDurationMid: '0.1s',
};

const Popover = {
   colorBgElevated: '#161a1d',
   colorText: 'rgb(240, 240, 240)',
};

const Pagination = {
   itemActiveBg: '',
   colorPrimary: 'rgb(240, 240, 240)',
   colorText: 'rgba(240, 240, 240)',
   colorPrimaryHover: 'rgba(240, 240, 240, .85)',
   colorTextDisabled: 'rgba(240, 240, 240, .6)',
};

const Checkbox = {
   colorBgContainer: '',
   colorPrimary: 'rgb(191, 0, 220)',
   colorPrimaryHover: 'rgba(191, 0, 220, .6)',
};

const List = {
   colorText: 'rgb(240, 240, 240)',
   colorBorder: 'rgba(240, 240, 240, .1)',
};

const Radio = {
   colorBgContainer: '',
   colorPrimary: 'rgb(191, 0, 220)',
   colorPrimaryHover: 'rgba(191, 0, 220, .6)',
   colorText: 'rgb(240, 240, 240)'
};

const Modal = {
   contentBg: '#161a1d',
   headerBg: '#161a1d',
   titleColor: '#fff',
   colorIcon: '#fff',
   colorIconHover: 'rgba(#fff, .7)'
}

export const config = {
   Input,
   Button,
   DatePicker,
   Popover,
   Pagination,
   Checkbox,
   List,
   Radio,
   Modal
};
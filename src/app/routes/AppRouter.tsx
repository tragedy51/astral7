import { Route, BrowserRouter as Router, Routes } from 'react-router-dom';

import { Dashboard } from '@/pages/dashboard';
import { Settings } from '@/pages/settings';
import { AddCard } from '@/pages/dashboard/add-card';
import { Tasks } from '@/pages/tasks';
import { Minds } from '@/pages/minds';
import { Auth } from '@/pages/auth';
import { Users } from '@/pages/users';
import { User } from '@/pages/user';
import { Layout } from '../layout';

export function AppRouter() {
   return (
      <Router>
         <Routes>
            <Route path={'/'} element={<Layout />}>
               <Route index element={<Dashboard />} />
               <Route path={':id/card'} element={<AddCard />} />
               <Route path={'tasks'} element={<Tasks />} />
               <Route path={'minds'} element={<Minds />} />
               <Route path={'users'} element={<Users />} />
               <Route path={'user/:id'} element={<User />} />
               <Route path={'settings'} element={<Settings />} />
            </Route>
            <Route path={'/auth'} element={<Auth />} />
         </Routes>
      </Router>
   );
}

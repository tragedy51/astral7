import { useEffect } from 'react';
import { observer } from 'mobx-react-lite';
import { Outlet, useNavigate } from 'react-router-dom';

import { Header } from '@/widgets/layout/header';
import { SideBar } from '@/widgets/layout/sidebar/index';
import { authApiStore } from '@/shared/store/auth-api';

export const Layout = observer(() => {
   const { get_me } = authApiStore;
   const navigate = useNavigate();

   const token = localStorage.getItem('token');

   useEffect(() => { !token ? navigate('/auth') : get_me() }, []);

   return (
      <>
         <Header />
         <div style={{ flex: 1 }} className="df">
            <SideBar />
            <main style={{ padding: 15 }} className="w100">
               <Outlet />
            </main>
         </div>
      </>
   );
})

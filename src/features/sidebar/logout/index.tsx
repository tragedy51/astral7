import { observer } from 'mobx-react-lite';
import { LogOut } from 'lucide-react';

import log from './index.module.scss';
import { authApiStore } from '@/shared/store/auth-api';
import { useNavigate } from 'react-router-dom'

export const Logout = observer(() => {
   const { logout } = authApiStore;
   const navigate = useNavigate();

   const handler = () => {
      logout();
      navigate('/auth');
   }

   return (
      <button onClick={handler} className={`${log.btn} w100 df jcc aic cw`}>
         <LogOut />
         Logout
      </button>
   );
})

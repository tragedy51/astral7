import { KeyboardEvent, useState } from 'react';
import { observer } from 'mobx-react-lite';
import { Pencil } from 'lucide-react';
import TextArea from 'antd/es/input/TextArea';

import { ISettingToUpdate } from '@/shared/interfaces/ISettings';
import desc from './index.module.scss';

interface props {
   description: string | undefined;
   updatePublicData: (data: ISettingToUpdate) => Promise<void>;
}

export const Description = observer(({ description, updatePublicData }: props) => {
   const [edit, setEdit] = useState(false);

   const uploadDescription = (e: KeyboardEvent<HTMLTextAreaElement>) => {
      if (e.key === 'Enter') {
         if (e.currentTarget.value !== description) {
            updatePublicData({
               key: 'description',
               value: e.currentTarget.value,
            });
         }
         setEdit(false);
      }
   };

   return (
      <div className={desc.container}>
         <div className="df gr10 aic">
            <h2>Description</h2>
            <Pencil
               className="cp"
               size={17}
               onClick={() => setEdit(prev => !prev)}
            />
         </div>
         {edit ? (
            <TextArea
               autoFocus
               autoSize
               defaultValue={description}
               maxLength={144}
               onKeyDown={uploadDescription}
            />
         ) : (
            <p>{description || 'Not specified :('}</p>
         )}
      </div>
   );
});

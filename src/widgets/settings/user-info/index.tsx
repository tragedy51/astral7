import { KeyboardEvent, useState } from 'react';
import { observer } from 'mobx-react-lite';
import { Input, Upload, UploadProps } from 'antd';
import { RcFile } from 'antd/es/upload';
import { Pencil, User } from 'lucide-react';

import { base64, beforeUpload, request } from '@/shared/utils/file-reader';
import { ISettingToUpdate } from '@/shared/interfaces/ISettings';
import { IUser } from '@/shared/interfaces/IUser';
import info from './index.module.scss';

interface props {
   user: IUser | undefined;
   updatePublicData: (data: ISettingToUpdate) => Promise<void>;
}
export const UserInfo = observer(({ user, updatePublicData }: props) => {
   const [edit, setEdit] = useState(false);

   const uploadAvatar: UploadProps['onChange'] = ({ file }) => {
      if (file.status === 'uploading') return;
      if (file.status === 'done') {
         base64(file.originFileObj as RcFile, url => {
            updatePublicData({ key: 'photoURL', value: url });
         });
      }
   };

   const uploadUsername = (e: KeyboardEvent<HTMLInputElement>) => {
      if (e.key === 'Enter') {
         if (user?.displayName !== e.currentTarget.value) {
            updatePublicData({ key: 'displayName', value: e.currentTarget.value });
         }
         setEdit(false);
      }
   };

   return (
      <div className={`${info.container} df fdc`}>
         <Pencil
            size={22}
            className={`${info.edit} cp`}
            onClick={() => setEdit(prev => !prev)}
         />
         <Upload
            name="avatar"
            beforeUpload={beforeUpload}
            customRequest={request}
            onChange={uploadAvatar}
            className="df jcc"
            listType="picture"
            showUploadList={false}>
            {user?.photoURL ? (
               <img
                  className="cp"
                  src={user.photoURL}
                  alt="avatar"
                  draggable={false}
               />
            ) : (
               <User size={80} color={'rgb(240, 240, 240)'} className="cp" />
            )}
         </Upload>
         {edit ? (
            <Input
               autoFocus
               maxLength={20}
               defaultValue={user?.displayName as string}
               onKeyDown={uploadUsername}
            />
         ) : (
            <h1>{user?.displayName || 'Not specified :('}</h1>
         )}
         <p>{user?.email}</p>
         <p>#{user?.uid}</p>
      </div>
   );
});

import { useState } from 'react';
import { Pencil } from 'lucide-react';
import { DatePicker, DatePickerProps, notification } from 'antd';
import dayjs from 'dayjs';

import date from './index.module.scss';
import { ISettingToUpdate } from '@/shared/interfaces/ISettings';
import { observer } from 'mobx-react-lite';
interface props {
   createdAt: string | undefined;
   birthday: string | undefined;
   updatePublicData: (data: ISettingToUpdate) => Promise<void>;
}

export const UserDates = observer(({ createdAt, birthday, updatePublicData }: props) => {
   const [open, setOpen] = useState(false);

   const updateBirthday: DatePickerProps['onChange'] = e => {
      if (e.toDate() > new Date()) {
         notification.error({ message: 'Your birthday should be less than today' });
         return;
      }

      if (e.format('DD.MM.YYYY') !== birthday) {
         updatePublicData({ key: 'birthday', value: e.format('DD.MM.YYYY') });
      }

      setOpen(false);
   };

   return (
      <div className={date.container}>
         <ul className="df aic jcsb">
            <li className="df aic gr10">
               Birthday:{' '}
               {open ? (
                  <DatePicker
                     size='small'
                     format={'DD.MM.YYYY'}
                     suffixIcon={null}
                     onChange={updateBirthday}
                  />
               ) : (
                  birthday || 'Not specified :('
               )}
               <Pencil
                  color={'rgb(240, 240, 240)'}
                  className="cp"
                  size={18}
                  onClick={() => setOpen(prev => !prev)}
               />
            </li>
            <li>Joined us: {dayjs(createdAt).format('DD.MM.YYYY') || 'Not found'}</li>
         </ul>
      </div>
   );
});

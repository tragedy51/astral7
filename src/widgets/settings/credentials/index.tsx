import { observer } from 'mobx-react-lite';
import { SubmitHandler, useForm } from 'react-hook-form';
import { zodResolver } from '@hookform/resolvers/zod';
import { Button } from 'antd';
import { Pencil } from 'lucide-react';
import { User } from 'firebase/auth';

import { ICredentials, credentialsSchema } from '@/shared/schemas/settings';
import cl from './index.module.scss';
import { FieldUi } from '@/shared/ui/field';

interface props {
   user: User | null;
   loading: boolean;
   updatePrivateData: (user: User, data: ICredentials) => Promise<void>;
}

export const Credentials = observer(
   ({ updatePrivateData, user, loading }: props) => {
      const {
         handleSubmit,
         formState: { errors },
         control,
      } = useForm<ICredentials>({
         resolver: zodResolver(credentialsSchema),
      });

      const submitData: SubmitHandler<ICredentials> = ({ email, password }) => {
         if (user) updatePrivateData(user, { email, password });
      };

      return (
         <form
            onSubmit={handleSubmit(submitData)}
            className={`${cl.form} df fdc`}>
            <div className="df aic gr10">
               <div className={cl.icon}>
                  <Pencil color="rgb(240, 240, 240)" />
               </div>
               <h2>Change your personal data</h2>
            </div>
            <FieldUi
               name="email"
               required={false}
               control={control}
               error={errors.email?.message}
               placeholder="Write new email"
            />
            <FieldUi
               name="password"
               required={false}
               control={control}
               error={errors.password?.message}
               placeholder="Write new password"
            />
            <Button
               disabled={loading}
               className="w100"
               htmlType="submit"
               type="primary">
               Change
            </Button>
         </form>
      );
   },
);

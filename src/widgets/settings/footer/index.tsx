import { Link } from 'react-router-dom';

import { GitlabIcon } from '@/shared/icons/GitlabIcon';
import { TelegramIcon } from '@/shared/icons/TelegramIcon';
import foot from './index.module.scss';

export function Footer() {
   return (
      <footer className={foot.container}>
         <ul style={{ gap: '0 7px' }} className="df">
            <li>
               <Link target="_blank" to={'https://gitlab.com/tragedy51'}>
                  <GitlabIcon />
               </Link>
            </li>
            <li>
               <Link target="_blank" to={'https://t.me/tragedyfiftyone'}>
                  <TelegramIcon />
               </Link>
            </li>
         </ul>
      </footer>
   );
}

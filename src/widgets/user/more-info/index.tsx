import { Cake, User } from 'lucide-react';

import { IUser } from '@/shared/interfaces/IUser';
import info from './index.module.scss';

export function MoreInfo({
   photoURL,
   uid,
   birthday,
   description,
}: Omit<IUser, 'email' | 'displayName'>) {
   return (
      <div>
         <div className="df jcc aic fdc">
            {photoURL ? (
               <img
                  className={info.avatar}
                  src={photoURL}
                  alt="avatar"
                  width={120}
                  height={120}
               />
            ) : (
               <User size={120} color="#fff" />
            )}
            <p className={info.id}>#{uid}</p>
         </div>
         <div className={info.cell}>
            {description && <p className={info.desc}>{description}</p>}
            {birthday && (
               <div className={`${info.birthday} df aic`}>
                  <Cake size={18} />
                  <p>{birthday}</p>
               </div>
            )}
         </div>
      </div>
   );
}

import { Button } from 'antd';
import { SubmitHandler, useForm } from 'react-hook-form';
import { zodResolver } from '@hookform/resolvers/zod';

import { ISignUp, signUpSchema } from '@/shared/schemas/auth';
import { FieldUi } from '@/shared/ui/field';
import form from './index.module.scss';

interface props {
   status: string | undefined;
   signUp: (
      email: string,
      password: string,
      username?: string,
   ) => Promise<void>;
}

export function SignUp({ signUp, status }: props) {
   const {
      formState: { errors },
      handleSubmit,
      control,
   } = useForm<ISignUp>({
      resolver: zodResolver(signUpSchema),
   });

   const auth: SubmitHandler<ISignUp> = ({ email, password, username }) => {
      signUp(email, password, username);
   };

   return (
      <form className="df fdc" onSubmit={handleSubmit(auth)}>
         <div className={`${form.fields} df fdc`}>
            <FieldUi
               showTitle
               name="username"
               required={false}
               control={control}
               placeholder="JohnDoe"
               error={errors.username?.message}
            />
            <FieldUi
               required
               showTitle
               name="email"
               control={control}
               error={errors.email?.message}
               placeholder="example@gmail.com"
            />
            <FieldUi
               required
               showTitle
               name="password"
               control={control}
               placeholder="JJssc56_!5A"
               error={errors.password?.message}
            />
            <FieldUi
               required
               showTitle
               control={control}
               name="confirmPassword"
               placeholder="JJssc56_!5A (Again)"
               error={errors.confirmPassword?.message}
            />
         </div>
         <Button
            disabled={status === 'pending'}
            type="primary"
            htmlType="submit"
            onSubmit={handleSubmit(auth)}
         >
            Sign Up
         </Button>
      </form>
   );
}

import { Button } from 'antd';
import { SubmitHandler, useForm } from 'react-hook-form';
import { zodResolver } from '@hookform/resolvers/zod';

import { ISignIn, signInSchema } from '@/shared/schemas/auth';
import { FieldUi } from '@/shared/ui/field';
import form from './index.module.scss';

interface props {
   status: string | undefined;
   signIn: (email: string, password: string) => Promise<void>;
}

export function SignIn({ signIn, status }: props) {
   const {
      formState: { errors },
      handleSubmit,
      control,
   } = useForm<ISignIn>({
      resolver: zodResolver(signInSchema),
   });

   const auth: SubmitHandler<ISignIn> = ({ email, password }) => signIn(email, password);

   return (
      <form className="df fdc" onSubmit={handleSubmit(auth)}>
         <div className={`${form.fields} df fdc`}>
            <FieldUi
               required
               showTitle
               name="email"
               control={control}
               error={errors.email?.message}
            />
            <FieldUi
               required
               showTitle
               name="password"
               control={control}
               error={errors.password?.message}
            />
         </div>
         <Button
            disabled={status === 'pending'}
            type="primary"
            htmlType="submit"
            onSubmit={handleSubmit(auth)}
         >
            Sign In
         </Button>
      </form>
   );
}

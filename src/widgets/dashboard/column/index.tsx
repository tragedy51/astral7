import { KeyboardEvent, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { Button, Input, Popover } from 'antd';
import { Grip, Plus } from 'lucide-react';

import { IColumnTask } from '@/shared/interfaces/IColumn';
import { TaskItem } from '@/entities/dashboard/task-item';
import { List } from '@/shared/ui/list';
import col from './index.module.scss';

interface props {
   uid: string;
   title: string;
   tasks: IColumnTask[] | null;
   deleteColumn: (uid: string) => Promise<void>;
   updateColumnTitle: (uid: string, obj: { title: string }) => Promise<void>;
}

export function Column({
   uid,
   title,
   tasks,
   deleteColumn,
   updateColumnTitle,
}: props) {
   const [open, setOpen] = useState(false);
   const [edit, setEdit] = useState(false);
   const navigate = useNavigate();

   const updateTitle = (e: KeyboardEvent<HTMLInputElement>) => {
      if (e.key === 'Enter') {
         if (e.currentTarget.value !== title) {
            updateColumnTitle(uid, { title: e.currentTarget.value });
         }

         setEdit(false);
      }
   };

   return (
      <>
         <div  className={`${col.container} cw`}>
            <div className="df aic jcsb">
               {edit ? (
                  <Input
                     className={col.input}
                     minLength={3}
                     maxLength={20}
                     defaultValue={title}
                     onKeyDown={updateTitle}
                  />
               ) : (
                  <h2 className={col.title}>{title}</h2>
               )}
               <button
                  className={'hov df aic'}
                  onClick={() => setOpen(prev => !prev)}>
                  <Grip />
               </button>
            </div>
            <div className={col.tasks}>
               <List
                  list={tasks?.sort((a, b) => a.priority.level > b.priority.level ? -1 : 1)}
                  callback={task => <TaskItem key={task.id} {...task} />}
               />
            </div>
            <button
               onClick={() => navigate(`${uid}/card`)}
               className={`${col.add} df aic w100 hov`}>
               <Plus size={18} />
               Add card
            </button>
         <Popover
            open={open}
            placement="right"
            content={
               <div className="df fdc">
                  <Button onClick={() => setEdit(prev => !prev)} type="primary">
                     Edit
                  </Button>
                  <Button
                     style={{ marginTop: 5 }}
                     type="primary"
                     onClick={() => deleteColumn(uid)}>
                     Delete
                  </Button>
               </div>
            }
         />
         </div>
      </>
   );
}

import { Image } from 'antd';

import { IColumnTask } from '@/shared/interfaces/IColumn';
import { TaskSideBar } from './sidebar';
import task from './index.module.scss';

export function TaskDetails({
   id,
   title,
   image,
   parentId,
   priority,
   completed,
   timestamps,
   description,
}: IColumnTask) {
   return (
      <div className={`${task.container} df`}>
         <div className={task.info}>
            <div className={task.image}>
               {image && (
                  <Image
                     src={image}
                     alt="task-image"
                     className={task.preview}
                  />
               )}
            </div>
            <p>{description}</p>
         </div>
         <TaskSideBar
            id={id}
            title={title}
            parentId={parentId}
            priority={priority}
            completed={completed}
            timestamps={timestamps}
         />
      </div>
   );
}

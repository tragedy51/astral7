import { Calendar, CircleX } from 'lucide-react';

import { IColumnTask } from '@/shared/interfaces/IColumn';
import side from './index.module.scss';
import { observer } from 'mobx-react-lite';
import { dashboardApiStore } from '@/shared/store/dashboard-api'

export const TaskSideBar = observer(({
   id,
   title,
   parentId,
   priority,
   completed,
   timestamps,
}: Omit<IColumnTask, 'image' | 'description'>) => {
      const { deleteTask } = dashboardApiStore;

      return (
         <div className={side.container}>
            <ul>
               <li className="hov">
                  <Calendar size={18} />
                  Dates
               </li>
               <li onClick={() => deleteTask(parentId, id)} className="hov">
                  <CircleX size={18} />
                  Delete
               </li>
            </ul>
         </div>
      );
   },
);

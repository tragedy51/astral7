import { Link, useLocation } from 'react-router-dom';

import { Logout } from '@/features/sidebar/logout';
import { navigation } from '@/shared/constants/navigation';
import side from './index.module.scss';

export function SideBar() {
   const { pathname } = useLocation();

   return (
      <aside className={side.container}>
         <nav>
            <ul>
               {navigation.map((link, i) => (
                  <li key={i}>
                     <Link
                        to={link.path}
                        className={
                           pathname === link.path
                              ? `${side.active} df aic cp`
                              : `${side.default} df aic cp hov`
                        }
                     >
                        {link.icon}
                        {link.text}
                     </Link>
                  </li>
               ))}
            </ul>
         </nav>
         <Logout />
      </aside>
   );
}

import { useState } from 'react';
import { Link } from 'react-router-dom';
import { Popover } from 'antd';
import { Menu } from 'lucide-react';

import { navigation } from '@/shared/constants/navigation';
import { Avatar } from '@/entities/header/avatar';
import { Logo } from '@/entities/logo';
import { Logout } from '@/features/sidebar/logout';
import head from './index.module.scss';

export function Header() {
   const [open, setOpen] = useState(false);

   return (
      <header className={`${head.container} df jcsb`}>
         <div className="df aic gr10">
            <Logo />
            <button className={head.menu} onClick={() => setOpen(true)}>
               <Menu color={'#fff'} />
            </button>
            <Popover
               open={open}
               placement="bottomRight"
               arrow={false}
               onOpenChange={() => setOpen(false)}
               content={
                  <div>
                     <nav>
                        <ul>
                           {navigation.map((link, i) => (
                              <li style={{ marginTop: 5 }} key={i}>
                                 <Link
                                    style={{ gap: '0 5px', fontWeight: 500 }}
                                    className="df aic cw"
                                    to={link.path}>
                                    {link.icon}
                                    {link.text}
                                 </Link>
                              </li>
                           ))}
                        </ul>
                     </nav>
                     <div style={{ marginTop: 5 }}>
                        <Logout />
                     </div>
                  </div>
               }
            />
         </div>
         <Avatar />
      </header>
   );
}

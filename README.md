<h1 align="center" id="title">Astral7</h1>

<p id="description">This is a project who's based on Trello app. Created by tragedy51</p>

<h2>🛠️ Installation Steps:</h2>

<p>1. clone this repository</p>

```
git clone https://gitlab.com/tragedy51/astral7.git
```

<p>2. install all dependencies</p>

```
yarn install
```

<p>3. run this project</p>

```
yarn dev
```
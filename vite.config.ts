import { defineConfig } from 'vite';
import react from '@vitejs/plugin-react';
import path from 'path';

// https://vitejs.dev/config/
export default defineConfig({
   plugins: [react()],
   resolve: {
      alias: {
         '@': path.resolve(__dirname, './src'),
         '@public': path.resolve(__dirname, './public')
      },
   },
   css: {
      preprocessorOptions: {
         scss: {
            additionalData: `@import "./src/app/assets/styles/variables";`
         }
      }
   }
});
